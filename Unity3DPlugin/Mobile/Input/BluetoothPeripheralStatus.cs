﻿namespace Icaros.Mobile.Input
{
    public enum BluetoothPeripheralStatus
    {
        /**
        * A GATT operation completed successfully
        */
        GATT_SUCCESS = 0,

        /**
        * The connection was terminated because of a L2C failure
        */
        GATT_CONN_L2C_FAILURE = 1,

        /**
        * The connection has timed out
        */
        GATT_CONN_TIMEOUT = 8,

        /**
        * GATT read operation is not permitted
        */
        GATT_READ_NOT_PERMITTED = 2,

        /**
        * GATT write operation is not permitted
        */
        GATT_WRITE_NOT_PERMITTED = 3,

        /**
        * Insufficient authentication for a given operation
        */
        GATT_INSUFFICIENT_AUTHENTICATION = 5,

        /**
        * The given request is not supported
        */
        GATT_REQUEST_NOT_SUPPORTED = 6,

        /**
        * Insufficient encryption for a given operation
        */
        GATT_INSUFFICIENT_ENCRYPTION = 15,

        /**
        * The connection was terminated by the peripheral
        */
        GATT_CONN_TERMINATE_PEER_USER = 19,

        /**
        * The connection was terminated by the local host
        */
        GATT_CONN_TERMINATE_LOCAL_HOST = 22,

        /**
        * The connection lost because of LMP timeout
        */
        GATT_CONN_LMP_TIMEOUT = 34,

        /**
        * The connection was terminated due to MIC failure
        */
        BLE_HCI_CONN_TERMINATED_DUE_TO_MIC_FAILURE = 61,

        /**
        * The connection cannot be established
        */
        GATT_CONN_FAIL_ESTABLISH = 62,

        /**
        * The peripheral has no resources to complete the request
        */
        GATT_NO_RESOURCES = 128,

        /**
        * Something went wrong in the bluetooth stack
        */
        GATT_INTERNAL_ERROR = 129,

        /**
        * The GATT operation could not be executed because the stack is busy
        */
        GATT_BUSY = 132,

        /**
        * Generic error, could be anything
        */
        GATT_ERROR = 133,

        /**
        * Authentication failed
        */
        GATT_AUTH_FAIL = 137,

        /**
        * The connection was cancelled
        */
        GATT_CONN_CANCEL = 256
    }
}