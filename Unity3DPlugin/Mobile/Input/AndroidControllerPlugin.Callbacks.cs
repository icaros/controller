﻿using System;
using System.Net.NetworkInformation;
using Icaros.SDK.Controller.Common;
using JetBrains.Annotations;

namespace Icaros.Mobile.Input
{
    public sealed partial class AndroidControllerPlugin
    {
        #region Callbacks invoked from Android Controller Plugin

        /// <summary>
        ///     Gets invoked when a new compatible controller has been found by the BLE scanner.
        /// </summary>
        /// <param name="peripheral">The peripheral identification information.</param>
        [UsedImplicitly]
        public void PeripheralFound(string peripheral)
        {
            var parts = peripheral.Split(ApiDelimiterChar);
            var deviceName = parts[0];
            // Convert "00:11:22:33:44:55" to "001122334455" format (.NET Standard 2.0 restriction)
            var address = PhysicalAddress.Parse(parts[1].Replace(":", string.Empty).ToUpper());

            var id = IcarosControllerIdentifier.CreateForBluetoothLowEnergy(address, deviceName);

            //
            // Add the device address to name lookup table
            // 
            if(!_devices.Contains(id))
                _devices.Add(id);
            
            OnControllerDiscovered?.Invoke(id);
        }

        /// <summary>
        ///     Gets invoked when a controller is in the process of becoming connected and ready.
        /// </summary>
        /// <param name="peripheral">The peripheral identification information.</param>
        [UsedImplicitly]
        public void PeripheralConnecting(string peripheral)
        {
            var parts = peripheral.Split(ApiDelimiterChar);
            var deviceName = parts[0];
            // Convert "00:11:22:33:44:55" to "001122334455" format (.NET Standard 2.0 restriction)
            var address = PhysicalAddress.Parse(parts[1].Replace(":", string.Empty).ToUpper());

            OnControllerConnecting?.Invoke(IcarosControllerIdentifier.CreateForBluetoothLowEnergy(address, deviceName));
        }

        /// <summary>
        ///     Gets invoked when a controller connection request has completed successfully.
        /// </summary>
        /// <param name="peripheral">The peripheral identification information.</param>
        [UsedImplicitly]
        public void PeripheralConnected(string peripheral)
        {
            var parts = peripheral.Split(ApiDelimiterChar);
            var deviceName = parts[0];
            // Convert "00:11:22:33:44:55" to "001122334455" format (.NET Standard 2.0 restriction)
            var address = PhysicalAddress.Parse(parts[1].Replace(":", string.Empty).ToUpper());

            var id = IcarosControllerIdentifier.CreateForBluetoothLowEnergy(address, deviceName);

            _currentDevice = id;

            OnControllerConnected?.Invoke(id);
        }

        /// <summary>
        ///     Gets invoked when a controller got disconnected.
        /// </summary>
        /// <param name="peripheral">The peripheral identification information.</param>
        [UsedImplicitly]
        public void PeripheralDisconnected(string peripheral)
        {
            var parts = peripheral.Split(ApiDelimiterChar);
            var deviceName = parts[0];
            // Convert "00:11:22:33:44:55" to "001122334455" format (.NET Standard 2.0 restriction)
            var address = PhysicalAddress.Parse(parts[1].Replace(":", string.Empty).ToUpper());
            var status = (BluetoothPeripheralStatus?) Enum.Parse(typeof(BluetoothPeripheralStatus), parts[2]);

            _currentDevice = null;

            OnControllerDisconnected?.Invoke(
                IcarosControllerIdentifier.CreateForBluetoothLowEnergy(address, deviceName),
                status
            );
        }

        /// <summary>
        ///     Gets invoked when an in-progress connection attempt has failed.
        /// </summary>
        /// <param name="peripheral">The peripheral identification information.</param>
        [UsedImplicitly]
        public void PeripheralConnectError(string peripheral)
        {
            var parts = peripheral.Split(ApiDelimiterChar);
            var deviceName = parts[0];
            // Convert "00:11:22:33:44:55" to "001122334455" format (.NET Standard 2.0 restriction)
            var address = PhysicalAddress.Parse(parts[1].Replace(":", string.Empty).ToUpper());
            var status = (BluetoothPeripheralStatus?) Enum.Parse(typeof(BluetoothPeripheralStatus), parts[2]);

            OnControllerConnectError?.Invoke(
                IcarosControllerIdentifier.CreateForBluetoothLowEnergy(address, deviceName),
                status
            );
        }

        #endregion
    }
}