﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using Icaros.SDK.Controller.Common;
using Icaros.SDK.Controller.Util;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Android;

namespace Icaros.Mobile.Input
{
    public sealed partial class AndroidControllerPlugin
    {
        /// <summary>
        ///     Attempt to initialize the Android library. Must be called at least once. Frequent calls during runtime reset the
        ///     internal state.
        /// </summary>
        /// <param name="debug">Instruct the Timber library to enable debug logging if true.</param>
        /// <returns>True on success, false if the required permissions are missing.</returns>
        [UsedImplicitly]
        public bool Initialize(bool debug = false)
        {
            if (!HasPermissions)
                return false;

            try
            {
                Debug.Log("Initializing native object");

                var jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");

                // Will throw an exception if BLE isn't properly available!
                _javaHandle = new AndroidJavaObject(
                    "com.icaros.IcarosController", // Class name
                    currentActivity, // Unity player activity
                    gameObject.name, // Required for UnitySendMessage (IPC) to work
                    debug // instruct Android library to log very verbose, if true
                );
            }
            catch (Exception ex)
            {
                _javaHandle = null;
                Debug.Log("Exception on initialization: " + ex);
            }

            return true;
        }

        /// <summary>
        ///     Prompt the user to grant the necessary runtime permissions.
        /// </summary>
        [UsedImplicitly]
        public void RequestPermissions()
        {
            if (!HasPermissions)
                Permission.RequestUserPermission(Permission.FineLocation);
        }

        /// <summary>
        ///     Start device enumeration. Implicitly calls <see cref="StartScan"/>.
        /// </summary>
        public void Enumerate()
        {
            StartScan();
        }

        /// <summary>
        ///     Instructs the BLE subsystem to scan for compatible devices within radio reach. This invalidates and drops all
        ///     previously discovered devices.
        /// </summary>
        /// <returns>True is the scan was successfully initiated, false otherwise.</returns>
        [UsedImplicitly]
        public bool StartScan()
        {
            _devices.Clear();

            return _javaHandle?.Call<bool>("startScan") ?? false;
        }

        /// <summary>
        ///     Stops an in-progress scan.
        /// </summary>
        [UsedImplicitly]
        public void StopScan()
        {
            _javaHandle?.Call("stopScan");
        }

        /// <summary>
        ///     Request connection to discovered device.
        /// </summary>
        /// <param name="peripheral">The <see cref="IcarosControllerIdentifier" /> identifying the remote device.</param>
        [UsedImplicitly]
        public void ConnectTo(IcarosControllerIdentifier peripheral)
        {
            _javaHandle?.Call("connectToPeripheral", peripheral.DeviceAddress.ToAddressString());
        }

        /// <summary>
        ///     Request connection to discovered device.
        /// </summary>
        /// <param name="address">The <see cref="PhysicalAddress" /> of the remote device.</param>
        [UsedImplicitly]
        public void ConnectTo(PhysicalAddress address)
        {
            _javaHandle?.Call("connectToPeripheral", address.ToAddressString());
        }

        /// <summary>
        ///     Request connection to discovered device.
        /// </summary>
        /// <param name="deviceName">
        ///     The name of the remote device. If multiple are within scanning range, the first one discovered
        ///     will be used. Use one of the overloaded methods to connect to a specific device.
        /// </param>
        /// <returns>True upon success, false if no device with such name has been discovered.</returns>
        [UsedImplicitly]
        public bool ConnectTo(string deviceName)
        {
            var id = Devices.FirstOrDefault(e =>
                e.DeviceName.Equals(deviceName, StringComparison.InvariantCultureIgnoreCase));

            if (id == null)
                return false;

            _javaHandle?.Call("connectToPeripheral", id.DeviceAddress.ToAddressString());

            return true;
        }

        /// <summary>
        ///     Request to disconnect all remote devices.
        /// </summary>
        [UsedImplicitly]
        public void DisconnectAll()
        {
            _javaHandle?.Call("disconnectFromPeripheral");
        }
    }
}