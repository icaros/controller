﻿using System;
using System.Collections.Generic;
using Icaros.Mobile.Util;
using Icaros.SDK.Controller.Common;
using Icaros.SDK.Controller.Windows;
using JetBrains.Annotations;

namespace Icaros.Mobile.Input
{
    /// <summary>
    ///     Unity wrapper around <see cref="WindowsUsbControllerEnumerator"/>.
    /// </summary>
    [UsedImplicitly]
    public sealed class WindowsUsbControllerPlugin : MonoBehaviourSingleton<WindowsUsbControllerPlugin>,
        IICarosControllerEnumerator
    {
        public WindowsUsbControllerPlugin()
        {
            //
            // Route events through to core enumerator
            // 
            WindowsUsbControllerEnumerator.Instance.OnControllerDiscovered +=
                identifier => OnControllerDiscovered?.Invoke(identifier);
            WindowsUsbControllerEnumerator.Instance.OnControllerConnecting +=
                identifier => OnControllerConnecting?.Invoke(identifier);
            WindowsUsbControllerEnumerator.Instance.OnControllerConnected +=
                identifier => OnControllerConnected?.Invoke(identifier);
            WindowsUsbControllerEnumerator.Instance.OnNewInputReportReceived += (identifier, bytes) =>
                OnNewInputReportReceived?.Invoke(identifier, bytes);
        }

        public IReadOnlyCollection<IcarosControllerIdentifier> Devices =>
            WindowsUsbControllerEnumerator.Instance.Devices;

        public event Action<IcarosControllerIdentifier> OnControllerDiscovered;
        public event Action<IcarosControllerIdentifier> OnControllerConnecting;
        public event Action<IcarosControllerIdentifier> OnControllerConnected;
        public event Action<IcarosControllerIdentifier, byte[]> OnNewInputReportReceived;

        public void Enumerate()
        {
            WindowsUsbControllerEnumerator.Instance.Enumerate();
        }

        public void ConnectTo(IcarosControllerIdentifier peripheral)
        {
            WindowsUsbControllerEnumerator.Instance.ConnectTo(peripheral);
        }

        public void DisconnectAll()
        {
            WindowsUsbControllerEnumerator.Instance.DisconnectAll();
        }
    }
}