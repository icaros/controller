﻿using System;
using Icaros.SDK.Controller.Common;
using JetBrains.Annotations;
using UnityEngine;

namespace Icaros.Mobile.Input
{
    /// <summary>
    ///     Parsed input report for use with Unity Engine.
    /// </summary>
    public class IcarosControllerInputReportUnity : IcarosControllerInputReport
    {
        public IcarosControllerInputReportUnity(byte[] buffer) : base(buffer)
        {
        }

        [UsedImplicitly]
        public static IIcarosControllerInputReport FromBuffer(byte[] buffer)
        {
            var report = new IcarosControllerInputReportUnity(buffer);
            report.TransformOrientation();
            return report;
        }

        /// <summary>
        ///     Call once before accessing axes/angles to compensate sensor orientation.
        /// </summary>
        /// <param name="orientation">
        ///     The <see cref="IcarosControllerDeviceOrientation" /> the controller is currently positioned
        ///     in.
        /// </param>
        [UsedImplicitly]
        public override void TransformOrientation(
            IcarosControllerDeviceOrientation orientation = IcarosControllerDeviceOrientation.VERTICAL)
        {
            /*
             * The default angles are used to compensate a potential
             * offset required to properly calculate the angle values.
             */
            var defaultAngles = new Quaternion().eulerAngles;

            /*
             * Four-byte to single-precision floating point number
             * conversion of reported byte buffer.
             */
            var qX = BitConverter.ToSingle(Buffer, 2);
            var qY = BitConverter.ToSingle(Buffer, 6);
            var qZ = BitConverter.ToSingle(Buffer, 10);
            var qW = BitConverter.ToSingle(Buffer, 14);

            /*
             * Rotation holds the current sensor position with the
             * previously defined offset (correction for position
             * in space) in consideration.
             */
            var rotation = new Quaternion();

            /*
             * The offset is required to "calibrate" the reporting sensor
             * orientation in space. With the following values added, a 
             * full angle on the X and Z axes is achieved when the device
             * is put in the default upright position where the pinkie of
             * the right hand of the operator touches the power button.
             */
            switch (orientation)
            {
                case IcarosControllerDeviceOrientation.VERTICAL:
                    rotation = new Quaternion(-0.5f, -0.5f, 0.5f, -0.5f) * new Quaternion(qX, qY, qZ, qW) *
                               Quaternion.Euler(0, 180, 0);
                    break;
                case IcarosControllerDeviceOrientation.HORIZONTAL:
                    rotation = new Quaternion(0.5f, -0.5f, -0.5f, -0.5f) * new Quaternion(-qY, -qX, qZ, -qW);
                    break;
            }

            /*
             * Angle values within the range of [0, 360] in degrees 
             * of the X, Y and Z axes.
             */
            var rotationAngles = rotation.eulerAngles;

            if (Mathf.Abs(rotationAngles.x - defaultAngles.x) > 1 / Sensitivity ||
                Mathf.Abs(rotationAngles.y - defaultAngles.y) > 1 / Sensitivity ||
                Mathf.Abs(rotationAngles.z - defaultAngles.z) > 1 / Sensitivity)
            {
                //
                // Transform coordinates to simple +/- 180° differences
                // 
                if (rotationAngles.x > 180f)
                    rotationAngles.x -= 360f;
                if (rotationAngles.y > 180f)
                    rotationAngles.y -= 360f;
                if (rotationAngles.z > 180f)
                    rotationAngles.z -= 360f;

                AngleX = rotationAngles.x;
                AngleY = rotationAngles.y;
                AngleZ = rotationAngles.z;
            }
        }
    }
}