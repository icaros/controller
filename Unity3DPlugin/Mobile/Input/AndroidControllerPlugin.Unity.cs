﻿using System;
using JetBrains.Annotations;

namespace Icaros.Mobile.Input
{
    public sealed partial class AndroidControllerPlugin
    {
        /// <summary>
        ///     Free resources on disposal.
        /// </summary>
        protected override void SingletonDestroyed()
        {
            StopScan();
            DisconnectAll();
        }

        /// <summary>
        ///     Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        [UsedImplicitly]
        void Update()
        {
            if (_javaHandle == null)
                return;

            var bytes = (byte[]) (Array) _javaHandle.Get<sbyte[]>("lastBytes");
            
            OnNewInputReportReceived?.Invoke(_currentDevice, bytes);
        }
    }
}