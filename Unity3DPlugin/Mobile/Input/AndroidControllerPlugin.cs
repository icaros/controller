﻿using System;
using System.Collections.Generic;
using System.Linq;
using Icaros.Mobile.Util;
using Icaros.SDK.Controller.Common;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Android;

namespace Icaros.Mobile.Input
{
    /// <summary>
    ///     Managed wrapper around the Android library handling Bluetooth Low Energy device discovery and connection states.
    /// </summary>
    public sealed partial class AndroidControllerPlugin : MonoBehaviourSingleton<AndroidControllerPlugin>, IICarosControllerEnumerator
    {
        private AndroidJavaObject _javaHandle;

        private readonly IList<IcarosControllerIdentifier> _devices;

        private IcarosControllerIdentifier _currentDevice;

        private AndroidControllerPlugin()
        {
            _devices = new List<IcarosControllerIdentifier>();
        }

        /// <summary>
        ///     Delimiter character used in the IPC callbacks.
        /// </summary>
        private static char ApiDelimiterChar => '|';

        #region Public poroperties

        /// <summary>
        ///     Collection of currently discovered devices.
        /// </summary>
        public IReadOnlyCollection<IcarosControllerIdentifier> Devices => _devices.ToList().AsReadOnly();
        
        /// <summary>
        ///     Gets the current status of the required permissions for BLE to work.
        /// </summary>
        [UsedImplicitly]
        public bool HasPermissions => Permission.HasUserAuthorizedPermission(Permission.FineLocation);

        #endregion

        #region Controller events to subscribe to

        /// <summary>
        ///     Gets fired when a compatible device has been found. Happens typically shortly after a scan has been started.
        /// </summary>
        public event Action<IcarosControllerIdentifier> OnControllerDiscovered;

        /// <summary>
        ///     Gets fired after a connection attempt has been instructed.
        /// </summary>
        public event Action<IcarosControllerIdentifier> OnControllerConnecting;

        /// <summary>
        ///     Gets fired after a connection request was successfully completed.
        /// </summary>
        public event Action<IcarosControllerIdentifier> OnControllerConnected;

        /// <summary>
        ///     Gets fired after a device has disconnected.
        /// </summary>
        public event Action<IcarosControllerIdentifier, BluetoothPeripheralStatus?> OnControllerDisconnected;

        /// <summary>
        ///     Gets fired if a connection attempt has failed due to an error.
        /// </summary>
        public event Action<IcarosControllerIdentifier, BluetoothPeripheralStatus?> OnControllerConnectError;

        /// <summary>
        ///     Gets fired when a new input state packet has been arrived for processing.
        /// </summary>
        public event Action<IcarosControllerIdentifier, byte[]> OnNewInputReportReceived;

        #endregion
    }
}