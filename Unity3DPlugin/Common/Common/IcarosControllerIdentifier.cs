﻿using System;
using System.Net.NetworkInformation;
using JetBrains.Annotations;

namespace Icaros.SDK.Controller.Common
{
    /// <summary>
    ///     Object for uniquely identifying an ICAROS Controller by taking connection protocol type into consideration.
    /// </summary>
    public sealed class IcarosControllerIdentifier : IEquatable<IcarosControllerIdentifier>
    {
        private IcarosControllerIdentifier(IcarosControllerCommunicationType type)
        {
            CommunicationType = type;
        }

        /// <summary>
        ///     Gets the communication protocol type this device is attached through.
        /// </summary>
        public IcarosControllerCommunicationType CommunicationType { get; }

        /// <summary>
        ///     Gets the remote device MAC address (only applies if connected through BLE).
        /// </summary>
        public PhysicalAddress DeviceAddress { get; private set; }

        /// <summary>
        ///     Product name of the controller.
        /// </summary>
        public string DeviceName { get; private set; }

        /// <summary>
        ///     Gets the serial number of the USB FTDI chip (only applies if connected on Windows through USB).
        /// </summary>
        public string SerialNumber { get; private set; }

        /// <summary>
        ///     Gets the name of the virtual COM-Port this device uses.
        /// </summary>
        public string ComPortName { get; private set; }

        /// <summary>
        ///     Create new identifier for BLE device.
        /// </summary>
        /// <param name="address">The remote device <see cref="PhysicalAddress" />.</param>
        /// <param name="deviceName">The device name.</param>
        /// <returns>The new <see cref="IcarosControllerIdentifier" />.</returns>
        [UsedImplicitly]
        public static IcarosControllerIdentifier CreateForBluetoothLowEnergy(PhysicalAddress address, string deviceName)
        {
            return new IcarosControllerIdentifier(IcarosControllerCommunicationType.BLUETOOTH_LOW_ENERGY)
            {
                DeviceAddress = address,
                DeviceName = deviceName
            };
        }

        /// <summary>
        ///     Create new identifier for USB device.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="deviceName">The device name.</param>
        /// <param name="serialPortName">Name of the COM-Port the device is connected through.</param>
        /// <returns>The new <see cref="IcarosControllerIdentifier" />.</returns>
        [UsedImplicitly]
        public static IcarosControllerIdentifier CreateForWindowsUsb(string serialNumber, string deviceName,
            string serialPortName)
        {
            return new IcarosControllerIdentifier(IcarosControllerCommunicationType.WINDOWS_USB)
            {
                SerialNumber = serialNumber,
                DeviceName = deviceName,
                ComPortName = serialPortName
            };
        }

        #region Equals

        public bool Equals(IcarosControllerIdentifier other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            switch (CommunicationType)
            {
                case IcarosControllerCommunicationType.BLUETOOTH_LOW_ENERGY:
                    return DeviceAddress.Equals(other.DeviceAddress);
                case IcarosControllerCommunicationType.WINDOWS_USB:
                    return SerialNumber.Equals(other.SerialNumber, StringComparison.InvariantCultureIgnoreCase);
                case IcarosControllerCommunicationType.UNKNOWN:
                default:
                    return false;
            }
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj) || obj is IcarosControllerIdentifier other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) CommunicationType;

                switch (CommunicationType)
                {
                    case IcarosControllerCommunicationType.BLUETOOTH_LOW_ENERGY:
                        hashCode = (hashCode * 397) ^ DeviceAddress.GetHashCode();
                        break;
                    case IcarosControllerCommunicationType.WINDOWS_USB:
                        hashCode = (hashCode * 397) ^ SerialNumber.GetHashCode();
                        break;
                    case IcarosControllerCommunicationType.UNKNOWN:
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                return hashCode;
            }
        }

        #endregion
    }
}