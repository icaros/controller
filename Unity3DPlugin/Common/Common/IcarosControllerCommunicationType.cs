﻿namespace Icaros.SDK.Controller.Common
{
    /// <summary>
    ///     Connection protocol used by controller.
    /// </summary>
    public enum IcarosControllerCommunicationType
    {
        /// <summary>
        ///     Undefined.
        /// </summary>
        UNKNOWN,
        /// <summary>
        ///     Bluetooth Low Energy (Android).
        /// </summary>
        BLUETOOTH_LOW_ENERGY,
        /// <summary>
        ///     Windows USB (FTDI).
        /// </summary>
        WINDOWS_USB
    }
}
