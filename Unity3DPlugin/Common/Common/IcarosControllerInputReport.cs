﻿using System;
using Icaros.SDK.Controller.Util;

namespace Icaros.SDK.Controller.Common
{
    /// <summary>
    ///     Represents an abstracted interpretation of the raw report bytes the controller sends.
    /// </summary>
    public abstract class IcarosControllerInputReport : IIcarosControllerInputReport
    {
        protected IcarosControllerInputReport(byte[] buffer)
        {
            Buffer = buffer;

            // Size check
            if (Buffer.Length != Length)
                throw new ArgumentException("Supplied buffer has an invalid length.");

            // Start byte check for additional safety
            if (Buffer[0] != Identifier)
                throw new ArgumentException("Supplied buffer doesn't start with expected packet identifier.");

            // Extract "o" button state
            IsCirclePressed = buffer[1].IsBitSet(0);

            // Extract "+" button state
            IsPlusPressed = buffer[1].IsBitSet(1);

            // Extract "-" button state
            IsMinusPressed = buffer[1].IsBitSet(2);

            // Extract "-o" button state
            IsPowerPressed = buffer[1].IsBitSet(3);
        }

        /// <summary>
        ///     Gets the expected packet identifier.
        /// </summary>
        public static byte Identifier => 0x55;

        /// <summary>
        ///     Gets the expected packet size.
        /// </summary>
        public static int Length => 20;

        /// <summary>
        ///     This is the inverse to the range of difference in angles to be registered. As of 09.11.16 the controller doesn't
        ///     seem to support anything lower then 100 or so.
        /// </summary>
        public static float Sensitivity => 250.0f;

        /// <summary>
        ///     True if the top button (circle) is pressed, false otherwise.
        /// </summary>
        public bool IsCirclePressed { get; }

        /// <summary>
        ///     True if the plus button is pressed, false otherwise.
        /// </summary>
        public bool IsPlusPressed { get; }

        /// <summary>
        ///     True if the minus button is pressed, false otherwise.
        /// </summary>
        public bool IsMinusPressed { get; }

        /// <summary>
        ///     True if the bottom button (power) is pressed, false otherwise.
        /// </summary>
        public bool IsPowerPressed { get; }

        /// <summary>
        ///     Gets the report buffer.
        /// </summary>
        public byte[] Buffer { get; protected set; }

        /// <summary>
        ///     Gets the transformed X-axis angle.
        /// </summary>
        public float AngleX { get; protected set; }

        /// <summary>
        ///     Gets the transformed Y-axis angle.
        /// </summary>
        public float AngleY { get; protected set; }

        /// <summary>
        ///     Gets the transformed Z-axis angle.
        /// </summary>
        public float AngleZ { get; protected set; }

        /// <summary>
        ///     Call once before accessing axes/angles to compensate sensor orientation.
        /// </summary>
        /// <param name="orientation">
        ///     The <see cref="IcarosControllerDeviceOrientation" /> the controller is currently positioned
        ///     in.
        /// </param>
        public abstract void TransformOrientation(
            IcarosControllerDeviceOrientation orientation = IcarosControllerDeviceOrientation.VERTICAL);

        /// <summary>
        ///     Gets the raw buffer as hex-notated string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return BitConverter.ToString(Buffer).Replace("-", " ");
        }
    }
}