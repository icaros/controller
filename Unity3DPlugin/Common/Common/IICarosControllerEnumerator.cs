﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Icaros.SDK.Controller.Common
{
    public interface IICarosControllerEnumerator
    {
        [UsedImplicitly]
        IReadOnlyCollection<IcarosControllerIdentifier> Devices { get; }

        [UsedImplicitly]
        void Enumerate();

        [UsedImplicitly]
        void ConnectTo(IcarosControllerIdentifier peripheral);

        [UsedImplicitly]
        void DisconnectAll();
        
        [UsedImplicitly]
        event Action<IcarosControllerIdentifier> OnControllerDiscovered;
        
        [UsedImplicitly]
        event Action<IcarosControllerIdentifier> OnControllerConnecting;
        
        [UsedImplicitly]
        event Action<IcarosControllerIdentifier> OnControllerConnected;
        
        [UsedImplicitly]
        event Action<IcarosControllerIdentifier, byte[]> OnNewInputReportReceived;
    }
}
