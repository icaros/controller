﻿using JetBrains.Annotations;

namespace Icaros.SDK.Controller.Common
{
    /// <summary>
    ///     Orientation of the sensor (in the physical device).
    /// </summary>
    public enum IcarosControllerDeviceOrientation
    {
        /// <summary>
        ///     Controller is mounted in upstanding position (e.g. on ICAROS handle).
        /// </summary>
        VERTICAL,

        /// <summary>
        ///     Controller is mounted in flat position (e.g. on ICAROS Race handle).
        /// </summary>
        HORIZONTAL
    }

    public interface IIcarosControllerInputReport
    {
        /// <summary>
        ///     True if the top button (circle) is pressed, false otherwise.
        /// </summary>
        [UsedImplicitly]
        bool IsCirclePressed { get; }

        /// <summary>
        ///     True if the plus button is pressed, false otherwise.
        /// </summary>
        [UsedImplicitly]
        bool IsPlusPressed { get; }

        /// <summary>
        ///     True if the minus button is pressed, false otherwise.
        /// </summary>
        [UsedImplicitly]
        bool IsMinusPressed { get; }

        /// <summary>
        ///     True if the bottom button (power) is pressed, false otherwise.
        /// </summary>
        [UsedImplicitly]
        bool IsPowerPressed { get; }

        /// <summary>
        ///     Gets the report buffer.
        /// </summary>
        [UsedImplicitly]
        byte[] Buffer { get; }

        /// <summary>
        ///     Gets the transformed X-axis angle.
        /// </summary>
        [UsedImplicitly]
        float AngleX { get; }

        /// <summary>
        ///     Gets the transformed Y-axis angle.
        /// </summary>
        [UsedImplicitly]
        float AngleY { get; }

        /// <summary>
        ///     Gets the transformed Z-axis angle.
        /// </summary>
        [UsedImplicitly]
        float AngleZ { get; }

        /// <summary>
        ///     Call once before accessing axes/angles to compensate sensor orientation.
        /// </summary>
        /// <param name="orientation">
        ///     The <see cref="IcarosControllerDeviceOrientation" /> the controller is currently positioned
        ///     in.
        /// </param>
        void TransformOrientation(
            IcarosControllerDeviceOrientation orientation = IcarosControllerDeviceOrientation.VERTICAL);
    }
}