﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FTD2XX_NET;
using Icaros.SDK.Controller.Common;
using Icaros.SDK.Controller.Util.FTDI;
using JetBrains.Annotations;

namespace Icaros.SDK.Controller.Windows
{
    /// <summary>
    ///     Find and connect to compatible devices connected via USB on Windows.
    /// </summary>
    public class WindowsUsbControllerEnumerator : IICarosControllerEnumerator
    {
        private const string FT_START_COMMAND = "START\r\n";

        private const string FT_IDENTIFY_COMMAND = "SERIAL?\r\n";

        private static readonly Lazy<WindowsUsbControllerEnumerator> LazyInstance =
            new Lazy<WindowsUsbControllerEnumerator>(() => new WindowsUsbControllerEnumerator());

        private readonly IDictionary<IcarosControllerIdentifier, IcarosSerialController> _devices;

        private WindowsUsbControllerEnumerator()
        {
            _devices = new Dictionary<IcarosControllerIdentifier, IcarosSerialController>();
        }

        /// <summary>
        ///     Gets the singleton instance of <see cref="WindowsUsbControllerEnumerator"/>.
        /// </summary>
        [UsedImplicitly] public static WindowsUsbControllerEnumerator Instance => LazyInstance.Value;

        private TimeSpan ReadWaitTimeSpan => TimeSpan.FromMilliseconds(100);

        private int IdentifyResponseExpectedBytes => 12;

        /// <summary>
        ///     Gets the expected identification prefix string of the device.
        /// </summary>
        public static string Identifier => "Icaros";

        public IReadOnlyCollection<IcarosControllerIdentifier> Devices => _devices.Keys.ToList().AsReadOnly();

        public event Action<IcarosControllerIdentifier> OnControllerDiscovered;
        public event Action<IcarosControllerIdentifier> OnControllerConnecting;
        public event Action<IcarosControllerIdentifier> OnControllerConnected;
        public event Action<IcarosControllerIdentifier, byte[]> OnNewInputReportReceived;

        public void Enumerate()
        {
            _devices.Clear();

            var sw = new Stopwatch();

            uint deviceCount = 0;
            var status = FTDI.FT_STATUS.FT_OK;

            // Create new instance of the FTDI device class
            var device = new FTDI();

            // Determine the number of FTDI devices connected to the machine
            status = device.GetNumberOfDevices(ref deviceCount);
            if (status != FTDI.FT_STATUS.FT_OK)
                throw new FTDIException("GetNumberOfDevices", status);

            // Allocate storage for device info list
            var deviceList = new FTDI.FT_DEVICE_INFO_NODE[deviceCount];

            // Populate our device list
            status = device.GetDeviceList(deviceList);
            if (status != FTDI.FT_STATUS.FT_OK)
                throw new FTDIException("GetDeviceList", status);

            //
            // Enumerate FTDI-compliant devices
            // 
            for (uint i = 0; i < deviceCount; i++)
            {
                status = device.OpenBySerialNumber(deviceList[i].SerialNumber);
                if (status != FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("OpenBySerialNumber", status);

                status = device.GetCOMPort(out var currentPort);
                if (status != FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("GetCOMPort", status);

                // Found, populate settings
                status = device.SetBaudRate(115200);
                if (status != FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("SetBaudRate", status);

                status = device.SetDataCharacteristics(
                    FTDI.FT_DATA_BITS.FT_BITS_8,
                    FTDI.FT_STOP_BITS.FT_STOP_BITS_1,
                    FTDI.FT_PARITY.FT_PARITY_NONE
                );
                if (status != FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("SetDataCharacteristics", status);

                status = device.SetFlowControl(
                    FTDI.FT_FLOW_CONTROL.FT_FLOW_RTS_CTS,
                    0x11,
                    0x13
                );
                if (status != FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("SetFlowControl", status);

                status = device.SetTimeouts(
                    2500,
                    2500
                );
                if (status != FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("SetTimeouts", status);

                var command = FT_IDENTIFY_COMMAND;
                uint numBytesWritten = 0;

                status = device.Write(command, command.Length, ref numBytesWritten);
                if (status != FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("Write", status);

                uint numBytesAvailable = 0;
                sw.Start();

                //
                // Primitive busy poll loop (the FTDI lib unfortunately doesn't provide an event)
                // 
                while (numBytesAvailable < IdentifyResponseExpectedBytes || sw.Elapsed < ReadWaitTimeSpan)
                {
                    status = device.GetRxBytesAvailable(ref numBytesAvailable);
                    if (status != FTDI.FT_STATUS.FT_OK)
                        throw new FTDIException("GetRxBytesAvailable", status);
                }

                sw.Stop();

                //
                // If these conditions aren't met, chances are high this isn't our device
                // 
                if (numBytesAvailable < IdentifyResponseExpectedBytes && sw.Elapsed >= ReadWaitTimeSpan)
                {
                    device.Close();
                    continue;
                }

                uint numBytesRead = 0;

                status = device.Read(out var identifier, numBytesAvailable, ref numBytesRead);
                if (status != FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("Read", status);

                // Match against string we expect
                if (identifier.StartsWith(Identifier))
                {
                    var id = IcarosControllerIdentifier.CreateForWindowsUsb(
                        deviceList[i].SerialNumber,
                        identifier.TrimEnd('\r', '\n'),
                        currentPort
                    );

                    _devices[id] = new IcarosSerialController(id, device);

                    OnControllerDiscovered?.Invoke(id);

                    //
                    // Continue enumerating, but keep handle open during object lifetime
                    // 
                    continue;
                }

                //
                // Not interested in this device, free underlying handle
                // 
                device.Close();
            }
        }

        public void ConnectTo(IcarosControllerIdentifier peripheral)
        {
            OnControllerConnecting?.Invoke(peripheral);

            //
            // Subscribe to event and route through to the outside
            // 
            _devices[peripheral].OnNewInputReportReceived += (controller, report) =>
            {
                OnNewInputReportReceived?.Invoke(controller.Id, report);
            };

            //
            // Instruct controller to start sending data
            // 
            _devices[peripheral].StartSendingReports();

            OnControllerConnected?.Invoke(peripheral);
        }

        public void DisconnectAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Meta-data about a discovered controller.
        /// </summary>
        protected class IcarosSerialController : IDisposable
        {
            public IcarosSerialController(IcarosControllerIdentifier id, FTDI deviceObject)
            {
                Id = id;
                DeviceObject = deviceObject;
                Cancellation = new CancellationTokenSource();

                Task.Factory.StartNew(SerialReaderWorker, Cancellation.Token);
            }

            public IcarosControllerIdentifier Id { get; }

            private FTDI DeviceObject { get; }

            private CancellationTokenSource Cancellation { get; }

            public void Dispose()
            {
                Cancellation.Cancel();
                Cancellation.Dispose();
                DeviceObject.Close();
            }

            public void StartSendingReports()
            {
                uint written = 0;
                DeviceObject.Write(FT_START_COMMAND, FT_START_COMMAND.Length, ref written);
            }

            public event Action<IcarosSerialController, byte[]> OnNewInputReportReceived;

            /// <summary>
            ///     Background task to keep reading from the device.
            /// </summary>
            /// <param name="obj">The <see cref="CancellationToken" /> to signal stopping.</param>
            private void SerialReaderWorker(object obj)
            {
                var token = (CancellationToken) obj;
                var receivedDataEvent = new AutoResetEvent(false);
                DeviceObject.SetEventNotification(FTDI.FT_EVENTS.FT_EVENT_RXCHAR, receivedDataEvent);

                while (!token.IsCancellationRequested)
                {
                    uint numBytesAvailable = 0;
                    var status = DeviceObject.GetRxBytesAvailable(ref numBytesAvailable);

                    if (numBytesAvailable == 0)
                        receivedDataEvent.WaitOne();

                    if (status == FTDI.FT_STATUS.FT_OK
                        && numBytesAvailable >= IcarosControllerInputReport.Length)
                    {
                        // Now that we have the amount of data we want available, read it
                        var buffer = new byte[IcarosControllerInputReport.Length];
                        uint numBytesRead = 0;
                        // Note that the Read method is overloaded, so can read string or byte array data
                        status = DeviceObject.Read(buffer, (uint) buffer.Length, ref numBytesRead);

                        if (buffer[0] == IcarosControllerInputReport.Identifier)
                            OnNewInputReportReceived?.Invoke(this, buffer);
                    }
                }
            }
        }
    }
}