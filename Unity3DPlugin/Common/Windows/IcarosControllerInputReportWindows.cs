﻿using System;
using System.Numerics;
using Icaros.SDK.Controller.Common;
using Icaros.SDK.Controller.Util;
using JetBrains.Annotations;

namespace Icaros.SDK.Controller.Windows
{
    /// <summary>
    ///     Parsed input report for use with .NET Core/Framework on Windows.
    /// </summary>
    public class IcarosControllerInputReportWindows : IcarosControllerInputReport
    {
        public IcarosControllerInputReportWindows(byte[] buffer) : base(buffer)
        {
        }

        [UsedImplicitly]
        public static IIcarosControllerInputReport FromBuffer(byte[] buffer)
        {
            var report = new IcarosControllerInputReportWindows(buffer);
            report.TransformOrientation();
            return report;
        }

        public override void TransformOrientation(
            IcarosControllerDeviceOrientation orientation = IcarosControllerDeviceOrientation.VERTICAL)
        {
            /*
            * The default angles are used to compensate a potential
            * offset required to properly calculate the angle values.
            */
            var defaultAngles = new Quaternion().ToVector3();

            /*
             * Four-byte to single-precision floating point number
             * conversion of reported byte buffer.
             */
            var qX = BitConverter.ToSingle(Buffer, 2);
            var qY = BitConverter.ToSingle(Buffer, 6);
            var qZ = BitConverter.ToSingle(Buffer, 10);
            var qW = BitConverter.ToSingle(Buffer, 14);

            /*
             * Rotation holds the current sensor position with the
             * previously defined offset (correction for position
             * in space) in consideration.
             */
            var rotation = new Quaternion();

            /*
             * The offset is required to "calibrate" the reporting sensor
             * orientation in space. With the following values added, a 
             * full angle on the X and Z axes is achieved when the device
             * is put in the default upright position where the pinkie of
             * the right hand of the operator touches the power button.
             */
            switch (orientation)
            {
                case IcarosControllerDeviceOrientation.VERTICAL:
                    rotation = new Quaternion(-0.5f, -0.5f, 0.5f, -0.5f) * new Quaternion(qX, qY, qZ, qW) *
                               Quaternion.CreateFromYawPitchRoll(0, 180, 0);
                    break;
                case IcarosControllerDeviceOrientation.HORIZONTAL:
                    rotation = new Quaternion(0.5f, -0.5f, -0.5f, -0.5f) * new Quaternion(-qY, -qX, qZ, -qW);
                    break;
            }

            /*
             * Angle values within the range of [0, 360] in degrees 
             * of the X, Y and Z axes.
             */
            var rotationAngles = rotation.ToVector3();

            if (Math.Abs(rotationAngles.X - defaultAngles.X) > 1 / Sensitivity ||
                Math.Abs(rotationAngles.Y - defaultAngles.Y) > 1 / Sensitivity ||
                Math.Abs(rotationAngles.Z - defaultAngles.Z) > 1 / Sensitivity)
            {
                //
                // Transform coordinates to simple +/- 180° differences
                // 
                if (rotationAngles.X > 180f)
                    rotationAngles.X -= 360f;
                if (rotationAngles.Y > 180f)
                    rotationAngles.Y -= 360f;
                if (rotationAngles.Z > 180f)
                    rotationAngles.Z -= 360f;

                AngleX = rotationAngles.X;
                AngleY = rotationAngles.Y;
                AngleZ = rotationAngles.Z;
            }
        }
    }
}