﻿using System.Linq;
using System.Net.NetworkInformation;

namespace Icaros.SDK.Controller.Util
{
    /// <summary>
    ///     Utility methods for use with <see cref="PhysicalAddress"/>.
    /// </summary>
    public static class PhysicalAddressExtensions
    {
        /// <summary>
        ///     Converts a <see cref="PhysicalAddress"/> into XX:XX:XX:XX:XX:XX notation.
        /// </summary>
        /// <param name="address">The <see cref="PhysicalAddress"/> to convert.</param>
        /// <returns>The resulting address string.</returns>
        public static string ToAddressString(this PhysicalAddress address)
        {
            return string.Join(":", (from z in address.GetAddressBytes() select z.ToString("X2")).ToArray());
        }
    }
}