﻿/*
 * BSD 3-Clause License
 * 
 * Copyright (c) 2019-2021, Nefarius Software Solutions e.U.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;

namespace Icaros.SDK.Controller.Util.FTDI
{
    public class FTDIException : Exception
    {
        public FTDIException(string message, FTD2XX_NET.FTDI.FT_STATUS status) : base(message)
        {
            Status = status;
        }

        public FTD2XX_NET.FTDI.FT_STATUS Status { get; }

        public override string ToString()
        {
            return $"{Message} failed with status {Enum.GetName(typeof(FTD2XX_NET.FTDI.FT_STATUS), Status)}";
        }
    }

    public static class FTDIEnumerator
    {
        public static FTD2XX_NET.FTDI GetByCOMPort(string port)
        {
            uint deviceCount = 0;
            var status = FTD2XX_NET.FTDI.FT_STATUS.FT_OK;

            // Create new instance of the FTDI device class
            var device = new FTD2XX_NET.FTDI();

            // Determine the number of FTDI devices connected to the machine
            status = device.GetNumberOfDevices(ref deviceCount);
            if (status != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                throw new FTDIException("GetNumberOfDevices", status);

            // Allocate storage for device info list
            var deviceList = new FTD2XX_NET.FTDI.FT_DEVICE_INFO_NODE[deviceCount];

            // Populate our device list
            status = device.GetDeviceList(deviceList);
            if (status != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                throw new FTDIException("GetDeviceList", status);

            for (uint i = 0; i < deviceCount; i++)
            {
                status = device.OpenBySerialNumber(deviceList[i].SerialNumber);
                if (status != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("OpenBySerialNumber", status);

                status = device.GetCOMPort(out var currentPort);
                if (status != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("GetCOMPort", status);
                
                if (!currentPort.Equals(port, StringComparison.InvariantCultureIgnoreCase)) continue;

                // Found, populate settings
                status = device.SetBaudRate(115200);
                if (status != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("SetBaudRate", status);

                status = device.SetDataCharacteristics(
                    FTD2XX_NET.FTDI.FT_DATA_BITS.FT_BITS_8, 
                    FTD2XX_NET.FTDI.FT_STOP_BITS.FT_STOP_BITS_1, 
                    FTD2XX_NET.FTDI.FT_PARITY.FT_PARITY_NONE
                );
                if (status != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("SetDataCharacteristics", status);

                status = device.SetFlowControl(
                    FTD2XX_NET.FTDI.FT_FLOW_CONTROL.FT_FLOW_RTS_CTS, 
                    0x11, 
                    0x13
                );
                if (status != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("SetFlowControl", status);

                status = device.SetTimeouts(
                    2500, 
                    2500
                );
                if (status != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                    throw new FTDIException("SetTimeouts", status);

                return device;
            }

            device.Close();
            return null;
        }
    }
}