﻿using System;
using System.Numerics;

namespace Icaros.SDK.Controller.Util
{
    internal static class QuaternionExtensions
    {
        private const float RadToDegree = (float) (180.0 / Math.PI);

        private const float Pi = (float) Math.PI;

        public static Vector3 ToVector3(this Quaternion q1)
        {
            var sqw = q1.W * q1.W;
            var sqx = q1.X * q1.X;
            var sqy = q1.Y * q1.Y;
            var sqz = q1.Z * q1.Z;
            var unit = sqx + sqy + sqz + sqw; // if normalized is one, otherwise is correction factor
            var test = q1.X * q1.W - q1.Y * q1.Z;
            Vector3 v;

            if (test > 0.4995f * unit)
            {
                // singularity at north pole
                v.Y = 2f * Atan2(q1.Y, q1.X);
                v.X = Pi / 2;
                v.Z = 0;
                return NormalizeAngles(v * RadToDegree);
            }

            if (test < -0.4995f * unit)
            {
                // singularity at south pole
                v.Y = -2f * Atan2(q1.Y, q1.X);
                v.X = -Pi / 2;
                v.Z = 0;
                return NormalizeAngles(v * RadToDegree);
            }

            var q = new Quaternion(q1.W, q1.Z, q1.X, q1.Y);
            v.Y = (float) Math.Atan2(2f * q.X * q.W + 2f * q.Y * q.Z, 1 - 2f * (q.Z * q.Z + q.W * q.W)); // Yaw
            v.X = (float) Math.Asin(2f * (q.X * q.Z - q.W * q.Y)); // Pitch
            v.Z = (float) Math.Atan2(2f * q.X * q.Y + 2f * q.Z * q.W, 1 - 2f * (q.Y * q.Y + q.Z * q.Z)); // Roll

            return NormalizeAngles(v * RadToDegree);
        }

        private static Vector3 NormalizeAngles(Vector3 angles)
        {
            angles.X = NormalizeAngle(angles.X);
            angles.Y = NormalizeAngle(angles.Y);
            angles.Z = NormalizeAngle(angles.Z);

            return angles;
        }

        /// <summary>
        ///     Clamp angle to a fixed range.
        /// </summary>
        /// <param name="angle">The original value.</param>
        /// <returns>The clamped value.</returns>
        private static float NormalizeAngle(float angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle < 0)
                angle += 360;

            return angle;
        }

        private static float Atan2(float y, float x)
        {
            return (float) Math.Atan2(y, x);
        }

        private static float Asin(float f)
        {
            return (float) Math.Asin(f);
        }
    }
}
