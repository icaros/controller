# Unity3D library for Windows

## Build setup

The library depends on managed types provided by the distributed Unity Editor. You must have at least one engine version installed or the project can't build. Get the exact version string from Unity Hub:

![unity_hub.png](unity_hub.png)

Edit `Unity3DPlugin.csproj` and update the version string in the `UnityVersion` element:

```xml
<UnityVersion>2020.3.30f1</UnityVersion>
```

Now building should work fine. If not, a Visual Studio restart might be required.

## Usage examples

When the controller gets attached to the system via USB for the first time, the [FTDI](https://ftdichip.com/) drivers and libraries get downloaded via Windows Update. This may take a while, depending on connection speed, so a small delay until the first detection succeeds is to be expected.

### .NET Framework

A simple console (or any other typo of .NET Framework/Core) application only needs a reference to the `Common` class library project (as you won't need nor want the dependency on Unity3D). Currently on Windows only USB-connected devices are supported. See example on how to enumerate, open and read from one or multiple controllers:

```csharp
using System;
using Icaros.SDK.Controller.Windows;

namespace DemoApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var enumerator = WindowsUsbControllerEnumerator.Instance;

            enumerator.OnNewInputReportReceived += (identifier, buffer) =>
            {
                var report = IcarosControllerInputReportWindows.FromBuffer(buffer);

                Console.WriteLine(report.AngleX);
            };

            enumerator.OnControllerDiscovered += identifier => { enumerator.ConnectTo(identifier); };

            enumerator.Enumerate();

            Console.ReadKey();
        }
    }
}
```

In this example, once a controller is discovered, a connection is established and the `OnNewInputReportReceived` event handler weill be invoked every time a state change has arrived. The helper class `IcarosControllerInputReportWindows` can be used to convert the raw report into easy-to-use properties, like Buttons and Axes.

### Unity3D on Android

The `Unity3DPlugin` project is a high-level wrapper and mandatory companion library to the `AndroidControllerPlugin`, both these projects must be provided as assets in the game's source files for it to work. For more details, check out the `AndroidControllerPlugin` directory.
