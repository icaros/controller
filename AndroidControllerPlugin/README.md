# Unity Plug-in for ICAROS Controller Support

## About

This Android library provides support for discovering, connecting and reading inputs from the ICAROS Controller using its Bluetooth Low Energy (BLE) capabilities. The library isn't interfaced directly by the engine, but the C# abstraction layer the `Unity3DPlugin` project provides. Both solutions are required for the Controller to work under Unity on Android.

## 3rd party credits

These additional dependencies are required to utilize this library:

- [BLESSED for Android - BLE made easy](https://github.com/weliem/blessed-android)
- [Timber Logger](https://github.com/JakeWharton/timber)

Some resources on the topic:

- [BLE in Android-Kotlin](https://medium.com/@nithinjith.p/ble-in-android-kotlin-c485f0e83c16)
- [Telit Terminal I/O Profile Client Implementation Guide](https://www.telit.com/wp-content/uploads/2017/09/TIO_Implementation_Guide_r6.pdf)
- [Making Android BLE work](https://medium.com/@martijn.van.welie/making-android-ble-work-part-1-a736dcd53b02)
