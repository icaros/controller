package com.icaros;

public interface IcarosControllerListener {
    void onDataReceived(String deviceName, String deviceAddress, byte[] report);
}
