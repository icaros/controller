package com.icaros;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.le.ScanResult;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;

import com.unity3d.player.UnityPlayer;
import com.welie.blessed.BluetoothBytesParser;
import com.welie.blessed.BluetoothCentral;
import com.welie.blessed.BluetoothCentralCallback;
import com.welie.blessed.BluetoothPeripheral;
import com.welie.blessed.BluetoothPeripheralCallback;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import timber.log.Timber;

import static android.bluetooth.BluetoothGatt.CONNECTION_PRIORITY_HIGH;
import static android.bluetooth.BluetoothGattCharacteristic.PROPERTY_WRITE;
import static android.bluetooth.BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT;
import static com.welie.blessed.BluetoothBytesParser.FORMAT_UINT8;
import static com.welie.blessed.BluetoothBytesParser.bytes2String;
import static com.welie.blessed.BluetoothPeripheral.GATT_SUCCESS;

public class IcarosController {

    // https://play.google.com/store/apps/details?id=com.telit.tiosample
    // https://www.telit.com/wp-content/uploads/2017/09/TIO_Implementation_Guide_r6.pdf
    private static final UUID BLUETOOTH_LE_TIO_SERVICE = UUID.fromString("0000FEFB-0000-1000-8000-00805F9B34FB");
    // UART Data RX Characteristics
    private static final UUID BLUETOOTH_LE_TIO_CHAR_TX = UUID.fromString("00000001-0000-1000-8000-008025000000"); // WNR
    // UART Data TX Characteristics
    private static final UUID BLUETOOTH_LE_TIO_CHAR_RX = UUID.fromString("00000002-0000-1000-8000-008025000000"); // N
    // UART Credits RX Characteristics
    private static final UUID BLUETOOTH_LE_TIO_CHAR_TX_CREDITS = UUID.fromString("00000003-0000-1000-8000-008025000000"); // W
    // UART Credits TX Characteristics
    private static final UUID BLUETOOTH_LE_TIO_CHAR_RX_CREDITS = UUID.fromString("00000004-0000-1000-8000-008025000000"); // I

    private static final int INITIAL_READ_CREDIT = 255;

    private static final String CONTROLLER_NAME_PREFIX = "Icaros";

    private static final String API_DELIMITER_CHAR = "|";

    public byte[] lastBytes = new byte[0];

    private BluetoothGattCharacteristic readCreditsCharacteristic = null;
    private BluetoothGattCharacteristic writeCreditsCharacteristic = null;
    private BluetoothGattCharacteristic readCharacteristic = null;
    private BluetoothGattCharacteristic writeCharacteristic = null;

    private List<IcarosControllerListener> listeners = new ArrayList<IcarosControllerListener>();
    /**
     * Gets called on peripheral connection, sets up services and characteristics
     */
    private final BluetoothPeripheralCallback peripheralCallback = new BluetoothPeripheralCallback() {

        // Keeps track of consumed/available read credits
        private int readCredits;

        @Override
        public void onServicesDiscovered(@NotNull BluetoothPeripheral peripheral) {
            Timber.i("Discovered services");

            peripheral.requestConnectionPriority(CONNECTION_PRIORITY_HIGH);

            // Optional
            //peripheral.readRemoteRssi();

            // Terminal I/O UART Service is what we need
            if (peripheral.getService(BLUETOOTH_LE_TIO_SERVICE) != null) {
                Timber.i("BLUETOOTH_LE_TIO_SERVICE service found");

                // Reset Credits
                readCredits = 0;

                readCreditsCharacteristic =
                        peripheral.getCharacteristic(
                                BLUETOOTH_LE_TIO_SERVICE,
                                BLUETOOTH_LE_TIO_CHAR_RX_CREDITS
                        );
                if (readCreditsCharacteristic != null) {
                    Timber.i("BLUETOOTH_LE_TIO_CHAR_RX_CREDITS found");
                    peripheral.setNotify(readCreditsCharacteristic, true);
                }

                writeCreditsCharacteristic =
                        peripheral.getCharacteristic(
                                BLUETOOTH_LE_TIO_SERVICE,
                                BLUETOOTH_LE_TIO_CHAR_TX_CREDITS
                        );
                if (writeCreditsCharacteristic != null) {
                    Timber.i("BLUETOOTH_LE_TIO_CHAR_TX_CREDITS found");

                    if ((writeCreditsCharacteristic.getProperties() & PROPERTY_WRITE) > 0) {
                        if ((peripheral.getName().contains(CONTROLLER_NAME_PREFIX))) {
                            Timber.i("Sending credits: %d", INITIAL_READ_CREDIT);
                            BluetoothBytesParser parser = new BluetoothBytesParser();
                            parser.setIntValue(INITIAL_READ_CREDIT, FORMAT_UINT8);
                            peripheral.writeCharacteristic(
                                    writeCreditsCharacteristic,
                                    parser.getValue(),
                                    WRITE_TYPE_DEFAULT
                            );
                        }
                    }
                }

                readCharacteristic =
                        peripheral.getCharacteristic(
                                BLUETOOTH_LE_TIO_SERVICE,
                                BLUETOOTH_LE_TIO_CHAR_RX
                        );
                if (readCharacteristic != null) {
                    Timber.i("BLUETOOTH_LE_TIO_CHAR_RX found");
                    peripheral.setNotify(readCharacteristic, true);
                }

                writeCharacteristic =
                        peripheral.getCharacteristic(
                                BLUETOOTH_LE_TIO_SERVICE,
                                BLUETOOTH_LE_TIO_CHAR_TX
                        );
                if (writeCharacteristic != null) {
                    Timber.i("BLUETOOTH_LE_TIO_CHAR_TX found");
                    //peripheral.setNotify(writeCharacteristic, true);
                }
            }
        }

        @Override
        public void onNotificationStateUpdate(@NotNull BluetoothPeripheral peripheral,
                                              @NotNull BluetoothGattCharacteristic characteristic,
                                              int status) {
            if (status == GATT_SUCCESS) {
                if (peripheral.isNotifying(characteristic)) {
                    Timber.i("SUCCESS: Notify set to 'on' for %s",
                            characteristic.getUuid());
                } else {
                    Timber.i("SUCCESS: Notify set to 'off' for %s",
                            characteristic.getUuid());
                }
            } else {
                Timber.e("ERROR: Changing notification state failed for %s",
                        characteristic.getUuid());
            }
        }

        /**
         * Recalculate and update the granted read credits to the remote device.
         * @param peripheral The peripheral to grant more credits for
         */
        private void grantReadCredits(BluetoothPeripheral peripheral) {
            final int minReadCredits = 16;
            final int maxReadCredits = 64;
            if (readCredits > 0)
                readCredits -= 1;
            if (readCredits <= minReadCredits) {
                int newCredits = maxReadCredits - readCredits;
                readCredits += newCredits;
                Timber.d("Grant read credits + %d = %d", newCredits, readCredits);
                BluetoothBytesParser parser = new BluetoothBytesParser();
                parser.setIntValue(newCredits, FORMAT_UINT8);
                peripheral.writeCharacteristic(
                        writeCreditsCharacteristic,
                        parser.getValue(),
                        WRITE_TYPE_DEFAULT
                );
            }
        }

        @Override
        public void onCharacteristicUpdate(
                @NotNull BluetoothPeripheral peripheral,
                @NotNull byte[] value,
                @NotNull BluetoothGattCharacteristic characteristic,
                int status
        ) {
            // Skip error case
            if (status != GATT_SUCCESS) return;

            // Identify input data characteristic
            if (characteristic == readCharacteristic) {
                //Timber.d("Input report received (length: %d)", value.length);

                // Validate expected packet format
                if (value.length >= 20 && value[0] == 0x55) {
                    // Invoke input report update
                    onInputReportReceived(peripheral, value);
                } else {
                    Timber.w("Unexpected packet format received");
                }

                // Calculate and update read credits
                grantReadCredits(peripheral);
            }

            if (characteristic == readCreditsCharacteristic) {
                BluetoothBytesParser parser = new BluetoothBytesParser(value);
                Timber.d("Read credits characteristic: %d",
                        parser.getIntValue(FORMAT_UINT8));
            }
        }

        @Override
        public void onCharacteristicWrite(@NotNull BluetoothPeripheral peripheral,
                                          @NotNull byte[] value,
                                          @NotNull BluetoothGattCharacteristic characteristic,
                                          int status) {
            if (status == GATT_SUCCESS) {
                Timber.i("SUCCESS: Writing <%s> to <%s>",
                        bytes2String(value),
                        characteristic.getUuid().toString());
            } else {
                Timber.i("ERROR: Failed writing <%s> to <%s>",
                        bytes2String(value),
                        characteristic.getUuid().toString());
            }
        }

        @Override
        public void onMtuChanged(@NotNull BluetoothPeripheral peripheral, int mtu, int status) {
            Timber.i("New MTU set: %d", mtu);
        }

        @Override
        public void onReadRemoteRssi(@NotNull BluetoothPeripheral peripheral, int rssi, int status) {
            Timber.i("Remote RSSI: %d", rssi);
        }
    };

    private Activity activity;

    private String callbackObjectName;

    private BluetoothCentral central;

    private final BluetoothCentralCallback bluetoothCentralCallback = new BluetoothCentralCallback() {
        @Override
        public void onDiscoveredPeripheral(BluetoothPeripheral peripheral, ScanResult scanResult) {
            Timber.i("Found peripheral '%s'", peripheral.getName());

            if (peripheral.getName().contains(CONTROLLER_NAME_PREFIX)) {
                Timber.d("%s device found", CONTROLLER_NAME_PREFIX);
                //central.stopScan();
                //central.connectPeripheral(peripheral, peripheralCallback);
                onPeripheralFound(peripheral);
            }
        }

        @Override
        public void onConnectedPeripheral(@NotNull BluetoothPeripheral peripheral) {
            Timber.i("Connected to '%s'", peripheral.getName());
            onConnected(peripheral);
        }

        @Override
        public void onConnectionFailed(@NotNull BluetoothPeripheral peripheral, int status) {
            Timber.e("Connection '%s' failed with status %d", peripheral.getName(), status);
            onConnectFailed(peripheral, status);
        }

        @Override
        public void onDisconnectedPeripheral(@NotNull BluetoothPeripheral peripheral, int status) {
            Timber.i("Disconnected '%s' with status %d", peripheral.getName(), status);
            onDisconnected(peripheral, status);
        }

        @Override
        public void onBluetoothAdapterStateChanged(int state) {
            Timber.i("Bluetooth adapter changed state to %d", state);
            if (state == BluetoothAdapter.STATE_ON) {
                // Bluetooth is on now, start scanning again
                // Scan for peripherals with a certain service UUIDs
                central.startPairingPopupHack();
                central.scanForPeripheralsWithServices(new UUID[]{BLUETOOTH_LE_TIO_SERVICE});
            }
        }
    };

    /**
     * Icaros Controller Scanner for Android BLE, identifies nearby Icaros Controller Devices
     *
     * @param currentActivity The parent activity (e.g. Unity object)
     * @param debug Enable verbose (debug) logging in true.
     * @throws IOException
     */
    public IcarosController(Activity currentActivity, String callbackObjectName, boolean debug) throws IOException {

        if (debug)
            Timber.plant(new Timber.DebugTree());

        Timber.plant(new Timber.DebugTree());

        // Keep reference to Activity
        this.activity = currentActivity;

        // Callback object name for Unity
        this.callbackObjectName = callbackObjectName;

        Timber.d("callbackObjectName: %s", callbackObjectName);

        // Check for necessary permissions and error out
        if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Timber.e("Missing required permission ACCESS_FINE_LOCATION");
            throw new IOException("Missing permissions to access location services (BLE)");
        }

        // Grab Bluetooth Central wrapper
        central = new BluetoothCentral(
                activity.getApplicationContext(),
                bluetoothCentralCallback,
                new Handler(Looper.getMainLooper())
        );

        if (!central.isBluetoothEnabled()) {
            Timber.e("Bluetooth not enabled");
            throw new IOException("Bluetooth must be enabled to use this functionality");
        }
    }

    /**
     * Start a scan for available BLE devices.
     *
     * @return True if successfully started scanning, False otherwise
     */
    public boolean startScan() {

        Timber.d("startScan");

        if (!central.isScanning())
            central.scanForPeripheralsWithServices(new UUID[]{BLUETOOTH_LE_TIO_SERVICE});

        return central.isScanning();
    }

    /**
     * Stop an ongoing scan, if any.
     */
    public void stopScan() {

        Timber.d("stopScan");

        if (central.isScanning())
            central.stopScan();
    }

    /**
     * Establish connection to specified peripheral
     *
     * @param address The MAC address of the peripheral
     */
    public void connectToPeripheral(String address) {

        BluetoothPeripheral peripheral = central.getPeripheral(address);

        central.autoConnectPeripheral(
                peripheral,
                peripheralCallback
        );

        UnityPlayer.UnitySendMessage(
                callbackObjectName,
                "PeripheralConnecting",
                peripheral.getName()
                        + API_DELIMITER_CHAR
                        + peripheral.getAddress()
        );

        Timber.d("connectToPeripheral: %s %s",
                peripheral.getName(), address);
    }

    /**
     * Disconnect from currently connected peripheral
     */
    public void disconnectFromPeripheral() {

        for (BluetoothPeripheral peripheral :
                central.getConnectedPeripherals()) {
            central.cancelConnection(peripheral);
        }

        Timber.d("disconnectFromPeripheral");
    }

    public void addListener(IcarosControllerListener listener) {
        listeners.add(listener);
    }

    /**
     * Called upon new input report arrival.
     *
     * @param peripheral The peripheral that sent the report
     * @param report     The raw report
     */
    protected void onInputReportReceived(BluetoothPeripheral peripheral, byte[] report) {
        //Timber.d("Input report received from %s", peripheral.getName());
        for (IcarosControllerListener listener : listeners) {
            listener.onDataReceived(peripheral.getName(), peripheral.getAddress(), report);
        }

        // Updated cached value
        lastBytes = report;
    }

    /**
     * New peripheral has been discovered.
     *
     * @param peripheral The discovered peripheral
     */
    protected void onPeripheralFound(BluetoothPeripheral peripheral) {

        UnityPlayer.UnitySendMessage(
                callbackObjectName,
                "PeripheralFound",
                peripheral.getName()
                        + API_DELIMITER_CHAR
                        + peripheral.getAddress()
        );

        Timber.i("onPeripheralFound");
    }

    /**
     * Successfully connected to a peripheral.
     *
     * @param peripheral The connected peripheral
     */
    protected void onConnected(BluetoothPeripheral peripheral) {

        stopScan();

        UnityPlayer.UnitySendMessage(
                callbackObjectName,
                "PeripheralConnected",
                peripheral.getName()
                        + API_DELIMITER_CHAR
                        + peripheral.getAddress()
        );

        Timber.i("onConnected");
    }

    /**
     * Report connection error.
     *
     * @param peripheral The affected peripheral
     * @param status     The error status code
     */
    public void onConnectFailed(@NotNull BluetoothPeripheral peripheral, int status) {

        UnityPlayer.UnitySendMessage(
                callbackObjectName,
                "PeripheralConnectError",
                peripheral.getName()
                        + API_DELIMITER_CHAR
                        + peripheral.getAddress()
                        + API_DELIMITER_CHAR
                        + String.valueOf(status)
        );

        Timber.d("PeripheralConnectError");
    }

    /**
     * Peripheral has disconnected.
     *
     * @param peripheral The peripheral that disconnected
     * @param status     The status of the disconnect
     */
    protected void onDisconnected(@NotNull BluetoothPeripheral peripheral, int status) {

        UnityPlayer.UnitySendMessage(
                callbackObjectName,
                "PeripheralDisconnected",
                peripheral.getName()
                        + API_DELIMITER_CHAR
                        + peripheral.getAddress()
                        + API_DELIMITER_CHAR
                        + String.valueOf(status)
        );

        Timber.i("onDisconnected");
    }

    /**
     * Call to clean-up used resources.
     */
    public void destroy() {

        if (central.isScanning()) {
            stopScan();
        }

        for (BluetoothPeripheral peripheral :
                central.getConnectedPeripherals()) {
            central.cancelConnection(peripheral);
        }

        Timber.d("destroy");
    }
}
