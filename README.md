# ICAROS Controller SDK

ICAROS Controller SDK for .NET, Unity and Android.

## About

This repository contains the middleware and SDK libraries needed to start communicating with the ICAROS motion controller on Android, Windows and Unity 3D.

The ICAROS Controller presents itself as an RS232-over-USB device when connected via USB, a Bluetooth SPP device on Bluetooth Classic or a SPP-over-GATT device on Bluetooth Low Energy (BLE). This SDK provides helpers and abstractions for device discovery, connection and reading on the following platforms/engines:

- .NET Framework on Windows via USB
- Unity3D on Windows via USB
- Unity3D on Android via wireless connection (BLE)

## Projects

The following sub-directories are provided:

### AndroidControllerPlugin

Unity3D plugin for Android. Requires *at least* **Android 6.0 (API level 23)** or newer. Can be edited and built with a recent version of [Android Studio](https://developer.android.com/studio/). This library takes care of interfacing with the OS-included Bluetooth APIs which are not available to Unity out-of-the-box. It's the mandatory middleware the `Unity3DPlugin` high-level abstractions require to read from the hardware. For details, see additional README.

### Unity3DPlugin

Unity3D/.NET Framework class library for Windows. Requires *at least* [Visual Studio](https://visualstudio.microsoft.com/downloads/) **2019 or newer** (Community Edition is fine too). Provides a class library for .NET Framework/Core projects and for Unity3D. The libraries take care of abstracting away the discovery, connection and reading process on Windows via USB using an FTDI wrapper and on Android via BLE. For details, see additional README.

## License

BSD 3-Clause License
